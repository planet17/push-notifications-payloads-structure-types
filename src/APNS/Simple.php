<?php
/**
 * Project: Push Notifications: Payloads Structure Types
 * Author:  planet17
 */


namespace Planet17\PushNotifications\PayloadsStructureTypes\APNS;


use Planet17\PushNotifications\Contracts\Pushes\Platform\IOS\{
    PushCategorizedContract as Subtitled,
    PushSubtitledContract as Categorized
};
use Planet17\PushNotifications\Pushes\Payloads\{
    APNS,
    Traits\IOSCategorized,
    Traits\IOSSubtitled
};

class Simple extends APNS implements Subtitled, Categorized
{
    use IOSCategorized, IOSSubtitled;
}
