<?php
/**
 * Project: Push Notifications: Payloads Structure Types
 * Author:  planet17
 */


namespace Planet17\PushNotifications\PayloadsStructureTypes\FCM;


use Planet17\PushNotifications\Pushes\Payloads\FCM;


/**
 * Class CustomWeb
 *
 * @package Planet17\PushNotifications\Common\Pushes
 */
class WebCustom extends FCM
{
    /** @inheritdoc */
    protected function constructPayload()
    {
        $postFields = [
            self::PROPERTY_CONTENT_AVAILABLE => $this->contentAvailable,
            self::PROPERTY_DRY_RUN           => $this->dryRun,
            self::PROPERTY_REGISTRATION_IDS  => $this->receivers->getTokens(),
            self::PROPERTY_TIME_TO_LIVE      => $this->timeToLive,
            self::PROPERTY_DATA      => [
                self::PROPERTY_TITLE        => $this->title,
                self::PROPERTY_BODY         => $this->body,
                self::PROPERTY_ICON         => $this->icon,
                self::PROPERTY_CLICK_ACTION => $this->getClickAction()
            ],
        ];

        if (null !== $this->tag) {
            $postFields[self::PROPERTY_DATA][self::PROPERTY_TAG] = $this->tag;
        }

        if (null !== $this->image) {
            $postFields[self::PROPERTY_DATA][self::PROPERTY_IMAGE] = $this->image;
        }

        if (null !== $this->actions) {
            $postFields[self::PROPERTY_DATA][self::PROPERTY_ACTIONS] = $this->actions->getActions();
        }

        $this->payload = json_encode($postFields);
    }
}
