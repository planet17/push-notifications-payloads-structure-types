<?php
/**
 * Project: Push Notifications: Payloads Structure Types
 * Author:  planet17
 */


namespace Planet17\PushNotifications\PayloadsStructureTypes\FCM;


use Planet17\PushNotifications\Pushes\Payloads\FCM;


class Simple extends FCM
{
    /** @inheritdoc */
    protected function constructPayload()
    {
        $postFields = [
            self::PROPERTY_CONTENT_AVAILABLE => $this->contentAvailable,
            self::PROPERTY_DRY_RUN           => $this->dryRun,
            self::PROPERTY_REGISTRATION_IDS  => $this->receivers->getTokens(),
            self::PROPERTY_TIME_TO_LIVE      => $this->timeToLive,
            self::PROPERTY_NOTIFICATION      => [
                self::PROPERTY_TITLE        => $this->title,
                self::PROPERTY_BODY         => $this->body,
                self::PROPERTY_ICON         => $this->icon,
                self::PROPERTY_CLICK_ACTION => $this->getClickAction(),
            ],
        ];

        $this->payload = json_encode($postFields);
    }
}
