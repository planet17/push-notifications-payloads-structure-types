<?php
/**
 * Project: Push Notifications: Payloads Structure Types
 * Author:  planet17
 */


namespace Planet17\PushNotifications\PayloadsStructureTypes\FCM;


use Planet17\PushNotifications\Contracts\Pushes\Platform\IOS\{
    PushSubtitledContract as Subtitled,
    PushCategorizedContract as Categorized
};
use Planet17\PushNotifications\Pushes\Payloads\{APNS, FCM};
use Planet17\PushNotifications\Pushes\Payloads\Traits\{IOSCategorized, IOSSubtitled};

/**
 * Class IosFCM
 *
 * @package Planet17\PushNotifications\Common\Pushes
 */
class IOS extends FCM implements Subtitled, Categorized
{
    use IOSCategorized, IOSSubtitled;


    /** @inheritdoc */
    protected function constructPayload()
    {
        $postFields = [
            self::PROPERTY_CONTENT_AVAILABLE => $this->contentAvailable,
            self::PROPERTY_DRY_RUN           => $this->dryRun,
            self::PROPERTY_REGISTRATION_IDS  => $this->receivers->getTokens(),
            self::PROPERTY_TIME_TO_LIVE      => $this->timeToLive,
            self::PROPERTY_NOTIFICATION      => [
                self::PROPERTY_TITLE        => $this->title,
                self::PROPERTY_BODY         => $this->body,
            ],
        ];


        if (null !== $this->subtitle) {
            $postFields[self::PROPERTY_NOTIFICATION][APNS::PROPERTY_SUBTITLE] = $this->subtitle;
        }

        if (null !== $this->category) {
            $postFields[self::PROPERTY_NOTIFICATION][self::PROPERTY_CLICK_ACTION] = $this->category;
        }

        $this->payload = json_encode($postFields);
    }
}
